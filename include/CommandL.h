#ifndef COMMANDL_H_
#define COMMANDL_H_

#include "anyoption.h"
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
    #include "omp.h"
#endif

//#include "anyoption.h"
//#include "Bus.h"
//#include "defs.h"
//#include "Generator.h"
//#include "Line.h"
//#include "MTRand.h"
//#include "Primes.h"
//#include "RandomNumbers.h"

namespace CommandL{
	extern void setUsage(AnyOption* opt);
    extern void setOptions(AnyOption* opt);
};

#endif
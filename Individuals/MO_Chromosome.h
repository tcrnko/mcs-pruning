/*
 * MO_Chromosome.h
 *
 *  Created on: Mar 29, 2011
 *      Author: rgreen
 */

#ifndef MO_CHROMOSOME_H_
#define MO_CHROMOSOME_H_

#include <vector>
#include <string>
class MO_Chromosome {
	public:
		MO_Chromosome();
		MO_Chromosome(int size);
		virtual ~MO_Chromosome();
		std::string toString();


		std::vector<int> 	pos;
		std::vector<double> fitness;
};

#endif /* MO_CHROMOSOME_H_ */

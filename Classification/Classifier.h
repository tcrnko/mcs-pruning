/*
 * Classifier.h
 *
 *  Created on: Jan 18, 2011
 *      Author: rgreen
 */

#ifndef CLASSIFIER_H_
#define CLASSIFIER_H_

#include <vector>
#include <string>

//#include "Utils.h"
#include "CStopWatch.h"
#include "Generator.h"
#include "Line.h"

#include "CommandL.h"
#include "getUtils.h"
#include "logging.h"
#include "mathUtils.h"
#include "PHEV.h"
#include "sampling.h"
#include "stringUtils.h"
#include "system.h"
#include "vector.h"  

using namespace CommandL;
using namespace GetUtils;
using namespace Logging;
using namespace MathUtils;
using namespace PHEV;
using namespace Sampling;
using namespace StringUtils;
using namespace System;
using namespace Vector;


class Classifier {
    public:
        Classifier();
        Classifier(const Classifier &c);
        Classifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b);
        Classifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > t);
        virtual ~Classifier();

        // virtual Classifier* create();
        virtual Classifier* clone();
        
        virtual Classifier & operator=(const Classifier &L); // assignment

        virtual void load();
        virtual void resetTimes();
        virtual void reset(){};
        virtual void init(){};

        virtual double run(std::string curSolution, double& excess);
        virtual double run(std::vector<int> curSolution, double& excess);
        virtual double run(std::vector<double> curSolution, double& excess);
        
        double getClassificationTime();
        double getLoadTime();
        double getResetTime();

        int                    getNumBuses();
        std::string            getCurSystem();
        std::vector<Generator> getGens();
        std::vector<Line>      getLines();
        std::vector<Bus>       getBuses();

        void setUseLines(bool b);

        void addLoad(std::vector<double>& amounts);
        void addLoad(int busNumber, double amount);

    protected:

        bool 		            useLines;
        CStopWatch              timer;
        double 	                loadTime, classificationTime, resetTime;

        int 					nb;
        std::string 			curSystem;
        std::vector<Generator> 	gens;
        std::vector<Line> 		lines;
        std::vector<Bus>		buses;
        std::vector<double>		busLoads, addedLoad;
};

#endif /* CLASSIFIER_H_ */

import sqlite3 as sql
import pystaggrelite3 as pyAgg
import fileinput
import pylab as p
import numpy as np
import matplotlib.ticker as ticker
from scipy.interpolate import griddata
import glob

from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d
from matplotlib import *
from matplotlib.lines import Line2D
from prettytable import *

from scipy import stats

# Pruners     = ['ACO', 'AIS', 'GA', 'PSO']
Systems     = ['RTS79', 'MRTS', 'RTS96']
# lineColors  = ["#A00000", "#00A000", "#5060D0", "#F25900", "#BB00D4"]
# lineMarkers = ['+', 'o', 'x', '^', 'D']
# figureDir   = "..\\Figures\\"


def func(x, pos):  # formatter function takes tick label and tick position
    s = str(x)
    s = s[0:len(s)-2]
    if len(s) > 3:
        s = s[0:len(s)-3] + "," + s[-3:]
    return s

def truncateTable(sql, outFile):
    con = sql.connect(outFile)
    cur = con.cursor()

    cur.execute("DROP TABLE IF EXISTS Samples;")

    cur.execute("CREATE TABLE IF NOT EXISTS Samples(ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                      RunNum INT, Population INT, Generations INT, SuccessStates INT, \
                                      FailureStates INT, StatesSlope DOUBLE, ProbSlope DOUBLE, \
                                      SamplerLOLP DOUBLE, PrunedLOLP DOUBLE, EstimateLOLP DOUBLE, \
                                      PruningTime DOUBLE, StateGenerationTime DOUBLE, SearchTime DOUBLE, \
                                      ClassificationTime DOUBLE, SimulationTime DOUBLE, TotalTime DOUBLE, \
                                      SamplerIterations INT, PruningIterations INT, Collisions INT, \
                                      AvgNumLinesOut INT, LineAdj DOUBLE, AvgNumGensOut DOUBLE, \
                                      PHEVPenetration DOUBLE, PHEVRho DOUBLE, numThreads INT, \
                                      System TEXT DEFAULT 'RTS79', Sampler TEXT DEFAULT 'MCS', \
                                      Classifier TEXT DEFAULT 'OPF', Pruner TEXT DEFAULT 'NONE', \
                                      PruningObjective TEXT DEFAULT 'NONE', LineFailures TEXT DEFAULT 'FALSE',\
                                      MultiObj TEXT DEFAULT 'FALSE', LocalSearch TEXT DEFAULT 'FALSE', \
                                      NegateFitness TEXT DEFAULT 'FALSE', PhevPlacement TEXT DEFAULT 'NONE', \
                                      BatchSize INT)")

    con.commit()

def importData(outFile, inFile, sql, drop = False):
    con = sql.connect(outFile)
    cur = con.cursor()

    if drop == True:
        cur.execute("DROP TABLE IF EXISTS Samples;")

        cur.execute("CREATE TABLE IF NOT EXISTS Samples(ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                      RunNum INT, Population INT, Generations INT, SuccessStates INT, \
                                      FailureStates INT, StatesSlope DOUBLE, ProbSlope DOUBLE, \
                                      SamplerLOLP DOUBLE, PrunedLOLP DOUBLE, EstimateLOLP DOUBLE, \
                                      PruningTime DOUBLE, StateGenerationTime DOUBLE, SearchTime DOUBLE, \
                                      ClassificationTime DOUBLE, SimulationTime DOUBLE, TotalTime DOUBLE, \
                                      SamplerIterations INT, PruningIterations INT, Collisions INT, \
                                      AvgNumLinesOut INT, LineAdj DOUBLE, AvgNumGensOut DOUBLE, \
                                      PHEVPenetration DOUBLE, PHEVRho DOUBLE, numThreads INT, \
                                      System TEXT DEFAULT 'RTS79', Sampler TEXT DEFAULT 'MCS', \
                                      Classifier TEXT DEFAULT 'OPF', Pruner TEXT DEFAULT 'NONE', \
                                      PruningObjective TEXT DEFAULT 'NONE', LineFailures TEXT DEFAULT 'FALSE',\
                                      MultiObj TEXT DEFAULT 'FALSE', LocalSearch TEXT DEFAULT 'FALSE', \
                                      NegateFitness TEXT DEFAULT 'FALSE', PhevPlacement TEXT DEFAULT 'NONE', \
                                      BatchSize INT)")
        print "Samples Table Created."


    insertSql = "INSERT INTO Samples VALUES(null,"

    fileName = inFile
    for line in fileinput.input(fileName):
        if not fileinput.isfirstline():
            # print "Inserting..."
            sql  = insertSql
            while line.find("  ") != -1:
                line = line.replace("  ", " ")
            line = line.rstrip()
            vals = line.split(" ")

            if len(vals) == 35:
                vals.insert(34, "None")
                # print "Vals[34]:" + vals[34]

            for s in vals:
                sql = (sql + "'" + s + "',")

            sql = sql[:-1] + ")"

            try:
                cur.execute(sql)
                # print fileName + ": Imported line " + str(fileinput.lineno())
            except Exception as e:
                print e
                print fileName
                print fileinput.lineno()
                print len(line.split(" "))
                print line
                print sql
                print ""
                raise e

    con.commit()
    con.close()

    print inFile + " imported."


def getStatData(sql, dbFile, system="RTS79", pruner="None", popSize=0, genLim=0):
    con          = sql.connect(dbFile)
    cur          = con.cursor()

    retValue = {}
    retValue['Time'] = []
    retValue['LOLP'] = []
    retValue['MCS Iterations'] = []

    if pruner == "NONE":
        sql =  "SELECT System, SamplerLOLP, TotalTime, SamplerIterations \
                FROM Samples \
                WHERE Pruner='%s' and System='%s' " % (pruner, system)
    else:

        if popSize == 0 or genLim == 0:
            sql =  "SELECT System, EstimateLOLP, TotalTime, SamplerIterations \
                    FROM Samples \
                    WHERE Pruner='%s' and System='%s'" % (pruner, system)
        else:
            sql =  "SELECT System, EstimateLOLP, TotalTime, SamplerIterations \
                    FROM Samples \
                    WHERE Pruner='%s' and System='%s' and Population=%i and Generations=%i" % (pruner, system, popSize, genLim)
    cur.execute(sql)
    results = cur.fetchall()
    con.commit()
    con.close()

    for r in results:
        retValue['LOLP'].append(float(r[1]))
        retValue['Time'].append(float(r[2]))
        retValue['MCS Iterations'].append(float(r[3]))

    retValue['Time']            = np.array(retValue['Time'])
    retValue['LOLP']            = np.array(retValue['LOLP'])
    retValue['MCS Iterations']  = np.array(retValue['MCS Iterations'])

    return retValue

def runStats(sql, dbFile):

    items    = ['LOLP', 'Time', 'MCS Iterations']
    prunerSamples = {}

    sysHeaders  = ['System', 'Item', 'Pruner', 'Pop', 'Gen', 'Min', 'Mean', 'Max', 'Variance', 'Skew', 'Kurtosis', 'Normal P', 'Mann-Whitney']

    for system in Systems:

        sysStats = getStatData(sql, dbFile, system=system, pruner="NONE")

        for item in items:
            sysTable = PrettyTable(sysHeaders)
            samples = []

            # Main System Stats
            n, (smin, smax), sm, sv, ss, sk = stats.describe(sysStats[item])
            t, p  = stats.normaltest(sysStats[item])
            samples.append(sysStats[item])
            #ks = stats.kstest(sysStats[item], 'norm')[1]
            mw = stats.mannwhitneyu(sysStats[item], sysStats[item])[1]
            curRow = [  system, item, "---", "---", "---",
                        "%2.4f" % smin, "%2.4f" % sm, "%2.4f" % smax, "%2.4f" % sv, "%2.4f" % ss, "%2.4f" % sk,
                        "%2.4f" % p, "%2.4f" % mw]
            sysTable.add_row(curRow)
            print (",").join(curRow)

            # Pruner Stats for each Pop/Gen
            for pruner in Pruners:
                prunerSamples[pruner] = getStatData(sql, dbFile, system=system, pruner=pruner)
                prunerStats           = prunerSamples[pruner]
                samples.append(prunerStats[item])
                n, (smin, smax), sm, sv, ss, sk = stats.describe(prunerStats[item])
                t, p   = stats.normaltest(prunerStats[item])
                # ks = stats.kstest(prunerStats[item], 'norm')[1]
                mw = stats.mannwhitneyu(sysStats[item], prunerStats[item])[1]
                curRow = [  system, item, pruner, "---", "---",
                            "%2.4f" % smin, "%2.4f" % sm, "%2.4f" % smax, "%2.4f" % sv, "%2.4f" % ss, "%2.4f" % sk,
                            "%2.4f" % p, "%2.4f" % mw]
                print (",").join(curRow)
                sysTable.add_row(curRow)

            print sysTable

            fig = pyplot.figure()
            pyplot.boxplot(samples)
            ax = fig.gca()
            labels = ('MCS', "ACO-ISSP", "AIS-ISSP", "GA-ISSP", "PSO-ISSP")
            xtickNames = pyplot.setp(ax, 'xticklabels', labels)
            ax.set_xlabel('Algorithm')
            ax.set_ylabel(item)
            pyplot.show()

            outFile = system + "_" + item + "_boxplot.png"
            fig.savefig(figureDir + outFile)
            print "Saved " + outFile
    return



    # fig = pyplot.figure()
    # pyplot.boxplot(samples)
    # labels = ('MCS', "AIS-ISSP", "ACO-ISSP", "GA-ISSP", "PSO-ISSP")
    # xtickNames = pyplot.setp(fig.gca(), 'xticklabels', labels)
    # pyplot.show()

def convergenceStats(sql, dbFile, popSize, genSize):
    con = sql.connect(dbFile)
    cur = con.cursor()

    con.create_aggregate("stdev", 1, pyAgg.stdev)
    sql = "SELECT System, Pruner, \
              AVG(EstimateLOLP), stdev(EstimateLOLP), \
              AVG(SamplerIterations), stdev(SamplerIterations), \
              AVG(TotalTime), stdev(TotalTime) \
              FROM Samples \
              WHERE Pruner<>'NONE' and Population=%i and Generations=%i \
              GROUP BY System, Pruner " % (popSize, genSize)
    cur.execute(sql)
    results = cur.fetchall()
    con.commit()
    con.close()

    headers = ["System", "Algorithm", "LOLP", "MCS Iterations", "Total Time (s)"]
    myTable = PrettyTable(headers)
    for row in results:
        curRow = []
        curRow.append(row[0])
        curRow.append(row[1])
        curRow.append(("%2.4f " + u"\u00B1" + " %2.4f") % (row[2], row[3]))
        curRow.append(("%2.2f " + u"\u00B1" + " %2.2f") % (row[4], row[5]))
        curRow.append(("%2.2f " + u"\u00B1" + " %2.2f") % (row[6], row[7]))
        myTable.add_row(curRow)

    print myTable

def basicStats(sql, dbFile, system="RTS79"):
    con          = sql.connect(dbFile)
    cur          = con.cursor()

    con.create_aggregate("stdev", 1, pyAgg.stdev)

    sql = "SELECT System, numThreads, BatchSize, \
                  AVG(SamplerLOLP),       stdev(SamplerLOLP), \
                  AVG(TotalTime),         stdev(TotalTime), \
                  AVG(SamplerIterations), stdev(SamplerIterations) \
                  FROM Samples \
                  WHERE System='%s' \
                  GROUP BY numThreads, BatchSize" % (system)

    cur.execute(sql)
    results = cur.fetchall()

    headers = ["System", "numThreads", "Batch Size", "LOLP", "Time (s)", "Iterations"]
    myTable = PrettyTable(headers)
    myTable.padding_width = 1

    for row in results:
        curRow = []
        curRow.append(row[0])
        curRow.append(row[1])
        curRow.append("{0:2,.0f}".format(row[2]))

        fLine = "{0:2,.4f} " + u"\u00B1" + " {0:2,.4f}"
        fLine = fLine.format(row[3], row[4])
        curRow.append(fLine)

        fLine = "{0:2,.2f} " + u"\u00B1" + " {0:2,.2f}"
        fLine = fLine.format(row[5], row[6])
        curRow.append(fLine)

        fLine = "{0:2,.0f} " + u"\u00B1" + " {0:2,.0f}"
        fLine = fLine.format(row[7], row[8])
        curRow.append(fLine)

        myTable.add_row(curRow)
    print myTable


    # headers  = ["System", "Pruner", "Total Time", "% Improv.", "Total", "% Improv.", "Pop. x Gen."]
    # myTable = PrettyTable(headers)
    # for system in Systems:
    #     for pruner in Pruners:
    #         sql = "SELECT System, Pruner, Population, Generations, \
    #                 AVG(TotalTime) as avgTime, stdev(TotalTime) as stdTime, \
    #                 AVG(SamplerIterations) as avgIters, stdev(SamplerIterations) as stdIters \
    #                 FROM Samples \
    #                 WHERE Pruner='%s' and System='%s'\
    #                 GROUP BY System, Pruner, Population, Generations \
    #                 ORDER BY AVG(TotalTime) \
    #                 Limit 1" % (pruner, system)
    #         cur.execute(sql)
    #         results = cur.fetchall()

    #         curRow = []
    #         curRow.append(results[0][0])
    #         curRow.append(results[0][1] + "-ISSP")

    #         # Time
    #         curRow.append(("%2.2f " + u"\u00B1" + " %2.2f") % (results[0][4], results[0][5]))

    #         if results[0][0] == "RTS79":
    #             curRow.append(("%2.2f") % ((1.0 - results[0][4]/rts79AvgTime) * 100.0))
    #         else:
    #             curRow.append(("%2.2f") % ((1.0 - results[0][4]/mrtsAvgTime) * 100.0))

    #         #Iterations
    #         curRow.append(("%2.2f " + u"\u00B1" + " %2.2f") % (results[0][6],results[0][7]))
    #         if results[0][0] == "RTS79":
    #             curRow.append("%2.2f" % ((1.0 - results[0][6]/rts79AvgIters) * 100))
    #         else:
    #             curRow.append("%2.2f" % ((1.0 - results[0][6]/mrtsAvgIters) * 100))

    #         curRow.append(("%i x %i") % (results[0][2],results[0][3]))
    #         myTable.add_row(curRow)

    print myTable
    con.commit()
    con.close()

def printDataTable(x, y, z):
    print "Not Implemented"

def surfacePlot(X, Y, Z, figureTitle, xAxisTitle, yAxisTitle, zAxisTitle, saveToFile="", formatter = ticker.FuncFormatter(func)):

    fig = pyplot.figure(figsize=(15, 8.5))
    ax  = fig.gca(projection='3d')
    xi  = np.linspace(X.min(), X.max(), 100)
    yi  = np.linspace(Y.min(), Y.max(), 100)
    zi  = griddata((X, Y), Z, (xi[None,:], yi[:,None]))

    xig, yig = np.meshgrid(xi, yi)
    surf = ax.plot_surface(xig, yig, zi, cmap=cm.gnuplot, linewidth=0, vmin=Z.min(), vmax=Z.max())
    fig.colorbar(surf, shrink=0.5)

    ax.yaxis.set_major_formatter(formatter)

    ax.set_title(figureTitle)
    ax.set_xlabel(xAxisTitle)
    ax.set_ylabel(yAxisTitle)
    ax.set_zlabel(zAxisTitle)

    if saveToFile == "":
        pyplot.show()
    else:
        pylab.savefig(saveToFile)


def surfacePlots(dbFile, sql, system="RTS79", batchLimit=20000):

    ###################################### Overall ######################################
    con = sql.connect(dbFile)
    cur = con.cursor()
    con.create_aggregate("stdev", 1, pyAgg.stdev)

    sql = "SELECT numThreads, batchSize, \
            AVG(TotalTime)          as TotalTimeAvg,    stdev(TotalTime)         as TotalTimeStdDev, \
            AVG(EstimateLOLP)       as lolpAvg,         stdev(EstimateLOLP)      as lolpStdDev, \
            AVG(SamplerIterations)  as itersAvg,        stdev(SamplerIterations) as itersStdDev \
            FROM Samples \
            WHERE System='%s' AND numThreads <= 12 AND batchSize < %i \
            GROUP BY numThreads, batchSize \
            ORDER BY numThreads, batchSize" % (system, batchLimit)

    cur.execute(sql)
    result1 = cur.fetchall()

    sql = "SELECT avg(TotalTime), System from Samples WHERE numThreads=1 AND System='%s'" %(system)

    cur.execute(sql)
    result2 = cur.fetchall()

    con.commit()
    con.close()

    nThreads = np.array([row[0] for row in result1], np.dtype('float64'))
    bSize    = np.array([row[1] for row in result1], np.dtype('float64'))
    time     = np.array([row[2] for row in result1], np.dtype('float64'))
    # lolp     = np.array([row[4] for row in results], np.dtype('float64'))
    # iters    = np.array([row[6] for row in result1], np.dtype('float64'))
    speedup  = np.array([result2[0][0]/row[2] for row in result1], np.dtype('float64'))
    eff      = speedup/nThreads

    surfacePlot(nThreads, bSize, time,    figureTitle=system, xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Time(s)')
    # surfacePlot(nThreads, bSize, iters,   figureTitle=system, xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Iterations')
    surfacePlot(nThreads, bSize, speedup, figureTitle=system, xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Speedup')
    surfacePlot(nThreads, bSize, eff,     figureTitle=system, xAxisTitle='# Threads', yAxisTitle='Batch Size', zAxisTitle='Efficiency')


def linePlots(dbFile, sql, system="RTS79", batchLimit=20000):

    con = sql.connect(dbFile)
    cur = con.cursor()
    con.create_aggregate("stdev", 1, pyAgg.stdev)

    sql = "SELECT numThreads, \
            AVG(TotalTime)          as TotalTimeAvg,    stdev(TotalTime)         as TotalTimeStdDev, \
            AVG(EstimateLOLP)       as lolpAvg,         stdev(EstimateLOLP)      as lolpStdDev, \
            AVG(SamplerIterations)  as itersAvg,        stdev(SamplerIterations) as itersStdDev \
            FROM Samples \
            WHERE System='%s' AND numThreads <= 12 AND batchSize < %i \
            GROUP BY numThreads \
            ORDER BY numThreads" % (system, batchLimit)

    cur.execute(sql)
    result1 = cur.fetchall()
    sql = "SELECT avg(TotalTime), System from Samples WHERE numThreads=1 AND System='%s'" %(system)

    cur.execute(sql)
    result2 = cur.fetchall()

    con.close()

    nThreads = np.array([row[0] for row in result1], np.dtype('float64'))
    time     = np.array([row[1] for row in result1], np.dtype('float64'))
    # iters    = np.array([row[3] for row in result1], np.dtype('float64'))
    speedup  = np.array([result2[0][0]/row[1] for row in result1], np.dtype('float64'))
    eff      = speedup/nThreads


    pyplot.plot(nThreads, time, color="red", lw=4, marker='+', ms=18, mew=2)
    pyplot.xlabel('# Threads')
    pyplot.ylabel('Avg. Time (s)')
    pyplot.title(system)
    pyplot.show()

    # pyplot.plot(nThreads, speedup, "bo--")
    # pyplot.xlabel('# Threads')
    # pyplot.ylabel('Speedup')
    # pyplot.title(system)
    # pyplot.show()

    # pyplot.plot(nThreads, eff, "go--")
    # pyplot.xlabel('# Threads')
    # pyplot.ylabel('Efficiency')
    # pyplot.title(system)
    # pyplot.show()


def importAllData(dbFile, sql, pattern="RTS79\\*.csv", truncTable=False):
    files = glob.glob(pattern)
    # print files

    if truncTable == True:
        truncateTable(sql, dbFile)

    # print "Importing " + str(len(files)) + " files"
    for f in files:
        importData(dbFile, f, sql)

if __name__ == "__main__":
    dbFile = 'Results.db'

    # importAllData(dbFile, sql, pattern="RTS79/*.csv", truncTable=True)
    # importAllData(dbFile, sql, pattern="MRTS/*.csv")
    # importAllData(dbFile, sql, pattern="RTS96/*.csv")

    # basicStats(sql, dbFile, system="RTS79")
    # basicStats(sql, dbFile, system="MRTS")
    # basicStats(sql, dbFile, system="RTS96")

    surfacePlots(dbFile, sql, system="RTS79", batchLimit=10000)
    # surfacePlots(dbFile, sql, system="MRTS", batchLimit=10000)
    # surfacePlots(dbFile, sql, system="RTS96", batchLimit=10000)

    linePlots(dbFile, sql, system="RTS79", batchLimit=10000)
    # linePlots(dbFile, sql, system="MRTS", batchLimit=10000)
    # linePlots(dbFile, sql, system="RTS96", batchLimit=10000)
#
# convergenceStats(sql, dbFile, 40, 100)
# runStats(sql, dbFile)

# importData(dbFile, 'GA_RTS79.csv', sql, True)
# importData(dbFile, 'PSO_RTS79.csv', sql)
# importData(dbFile, 'ACO_RTS79.csv', sql)
# importData(dbFile, 'AIS_RTS79.csv', sql)
# importData(dbFile, 'MCS_RTS79.csv', sql)

# importData(dbFile, 'GA_MRTS.csv', sql)
# importData(dbFile, 'PSO_MRTS.csv', sql)
# importData(dbFile, 'ACO_MRTS.csv', sql)
# importData(dbFile, 'AIS_MRTS.csv', sql)
# importData(dbFile, 'MCS_MRTS.csv', sql)

# surfacePlots(dbFile, sql, pyAgg, pruner="GA",  system="RTS79")
# surfacePlots(dbFile, sql, pyAgg, pruner="PSO", system="RTS79")
# surfacePlots(dbFile, sql, pyAgg, pruner="ACO", system="RTS79")
# surfacePlots(dbFile, sql, pyAgg, pruner="AIS", system="RTS79")

# surfacePlots(dbFile, sql, pyAgg, pruner="GA",  system="MRTS")
# surfacePlots(dbFile, sql, pyAgg, pruner="PSO", system="MRTS")
# surfacePlots(dbFile, sql, pyAgg, pruner="ACO", system="MRTS")
# surfacePlots(dbFile, sql, pyAgg, pruner="AIS", system="MRTS")

# linePlots(dbFile, sql, pyAgg, system="RTS79", field="TotalTime", label="Time (s)", includeAis=False, outFile="RTS79_NoLines_Time.png")
# linePlots(dbFile, sql, pyAgg, system="RTS79", field="SuccessStates", label="Success States Pruned", includeAis=False, outFile="RTS79_NoLines_SS.png")
# linePlots(dbFile, sql, pyAgg, system="RTS79", field="SamplerIterations", label="MCS Iterations", includeAis=False, outFile="RTS79_NoLines_Iters.png")

# linePlots(dbFile, sql, pyAgg, system="MRTS", field="TotalTime", label="Time (s)", includeAis=False, outFile="MRTS_NoLines_Time.png")
# linePlots(dbFile, sql, pyAgg, system="MRTS", field="SuccessStates", label="Success States Pruned", includeAis=False, outFile="MRTS_NoLines_SS.png")
# linePlots(dbFile, sql, pyAgg, system="MRTS", field="SamplerIterations", label="MCS Iterations", includeAis=False, outFile="MRTS_NoLines_Iters.png")

# linePlots(dbFile, sql, pyAgg, system="RTS79", field="TotalTime",        label="Time (s)", includeAis=True, outFile="RTS79_NoLines_Time_AIS.png")
# linePlots(dbFile, sql, pyAgg, system="RTS79", field="SuccessStates",    label="Success States Pruned", includeAis=True, outFile="RTS79_NoLines_SS_AIS.png")
# linePlots(dbFile, sql, pyAgg, system="RTS79", field="SamplerIterations",label="MCS Iterations", includeAis=True, outFile="RTS79_NoLines_Iters_AIS.png")
# linePlots(dbFile, sql, pyAgg, system="RTS79", field="Collisions",       label="Collisions", includeAis=True, outFile="RTS79_NoLines_Collisions_AIS.png")

# linePlots(dbFile, sql, pyAgg, system="MRTS", field="TotalTime",         label="Time (s)", includeAis=True, outFile="MRTS_NoLines_Time_AIS.png")
# linePlots(dbFile, sql, pyAgg, system="MRTS", field="SuccessStates",     label="Success States Pruned", includeAis=True, outFile="MRTS_NoLines_SS_AIS.png")
# linePlots(dbFile, sql, pyAgg, system="MRTS", field="SamplerIterations", label="MCS Iterations", includeAis=True, outFile="MRTS_NoLines_Iters_AIS.png")
# linePlots(dbFile, sql, pyAgg, system="MRTS", field="Collisions",       label="Collisions", includeAis=True, outFile="MRTS_NoLines_Collisions_AIS.png")
